package com.netcracker.ec.khudnitsky.lect_04.command.commands;

import com.netcracker.ec.khudnitsky.lect_04.command.editor.Editor;

public abstract class Command {
  public Editor editor;
  private String backup;

  protected Command(Editor editor) {
    this.editor = editor;
  }

  protected void backup() {
    backup = editor.textField.getText();
  }

  public void undo() {
    editor.textField.setText(backup);
  }

  public abstract boolean execute();
}
