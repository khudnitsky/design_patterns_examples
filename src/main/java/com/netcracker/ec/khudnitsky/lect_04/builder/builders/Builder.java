package com.netcracker.ec.khudnitsky.lect_04.builder.builders;

import com.netcracker.ec.khudnitsky.lect_04.builder.entities.Type;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.Engine;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.GPSNavigator;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.Transmission;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.TripComputer;

/**
 * Интерфейс Строителя объявляет все возможные этапы и шаги конфигурации
 * продукта.
 */
public interface Builder {
  void setType(Type type);
  void setSeats(int seats);
  void setEngine(Engine engine);
  void setTransmission(Transmission transmission);
  void setTripComputer(TripComputer tripComputer);
  void setGPSNavigator(GPSNavigator gpsNavigator);
}
