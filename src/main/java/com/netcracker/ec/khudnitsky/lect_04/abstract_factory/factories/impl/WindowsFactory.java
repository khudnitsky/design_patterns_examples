package com.netcracker.ec.khudnitsky.lect_04.abstract_factory.factories.impl;

import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.buttons.Button;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.buttons.impl.WindowsButton;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.checkboxes.Checkbox;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.checkboxes.impl.WindowsCheckbox;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.factories.GUIFactory;

/**
 * Каждая конкретная фабрика знает и создаёт только продукты своей вариации.
 */
public class WindowsFactory implements GUIFactory {

  @Override
  public Button createButton() {
    return new WindowsButton();
  }

  @Override
  public Checkbox createCheckbox() {
    return new WindowsCheckbox();
  }
}
