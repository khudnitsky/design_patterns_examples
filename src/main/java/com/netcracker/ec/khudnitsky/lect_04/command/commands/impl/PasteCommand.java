package com.netcracker.ec.khudnitsky.lect_04.command.commands.impl;

import com.netcracker.ec.khudnitsky.lect_04.command.commands.Command;
import com.netcracker.ec.khudnitsky.lect_04.command.editor.Editor;

public class PasteCommand extends Command {

  public PasteCommand(Editor editor) {
    super(editor);
  }

  @Override
  public boolean execute() {
    if (editor.clipboard == null || editor.clipboard.isEmpty()) return false;

    backup();
    editor.textField.insert(editor.clipboard, editor.textField.getCaretPosition());
    return true;
  }
}
