package com.netcracker.ec.khudnitsky.lect_04.command;

import com.netcracker.ec.khudnitsky.lect_04.command.editor.Editor;

public class Runner {
  public static void main(String[] args) {
    Editor editor = new Editor();
    editor.init();
  }
}
