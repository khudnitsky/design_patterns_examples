package com.netcracker.ec.khudnitsky.lect_04.factory_method.factories.impl;

import com.netcracker.ec.khudnitsky.lect_04.factory_method.buttons.Button;
import com.netcracker.ec.khudnitsky.lect_04.factory_method.buttons.impl.HtmlButton;
import com.netcracker.ec.khudnitsky.lect_04.factory_method.factories.Dialog;

/**
 * HTML-диалог.
 */
public class HtmlDialog extends Dialog {

  @Override
  public Button createButton() {
    return new HtmlButton();
  }
}
