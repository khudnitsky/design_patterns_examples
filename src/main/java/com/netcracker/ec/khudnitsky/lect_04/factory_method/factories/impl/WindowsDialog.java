package com.netcracker.ec.khudnitsky.lect_04.factory_method.factories.impl;

import com.netcracker.ec.khudnitsky.lect_04.factory_method.buttons.Button;
import com.netcracker.ec.khudnitsky.lect_04.factory_method.buttons.impl.WindowsButton;
import com.netcracker.ec.khudnitsky.lect_04.factory_method.factories.Dialog;

/**
 * Диалог на элементах операционной системы.
 */
public class WindowsDialog extends Dialog {

  @Override
  public Button createButton() {
    return new WindowsButton();
  }
}
