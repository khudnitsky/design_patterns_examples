package com.netcracker.ec.khudnitsky.lect_04.builder.directors;

import com.netcracker.ec.khudnitsky.lect_04.builder.builders.Builder;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.Type;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.Engine;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.GPSNavigator;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.Transmission;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.TripComputer;

/**
 * Директор знает в какой последовательности заставлять работать строителя. Он
 * работает с ним через общий интерфейс Строителя. Из-за этого, он может не
 * знать какой конкретно продукт сейчас строится.
 */
public class Director {

  public void constructSportsCar(Builder builder) {
    builder.setType(Type.SPORTS_CAR);
    builder.setSeats(2);
    builder.setEngine(new Engine(3.0, 0));
    builder.setTransmission(Transmission.SEMI_AUTOMATIC);
    builder.setTripComputer(new TripComputer());
    builder.setGPSNavigator(new GPSNavigator());
  }

  public void constructCityCar(Builder builder) {
    builder.setType(Type.CITY_CAR);
    builder.setSeats(2);
    builder.setEngine(new Engine(1.2, 0));
    builder.setTransmission(Transmission.AUTOMATIC);
    builder.setTripComputer(new TripComputer());
    builder.setGPSNavigator(new GPSNavigator());
  }

  public void constructSUV(Builder builder) {
    builder.setType(Type.SUV);
    builder.setSeats(4);
    builder.setEngine(new Engine(2.5, 0));
    builder.setTransmission(Transmission.MANUAL);
    builder.setGPSNavigator(new GPSNavigator());
  }
}
