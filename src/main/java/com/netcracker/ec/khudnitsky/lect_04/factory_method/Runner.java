package com.netcracker.ec.khudnitsky.lect_04.factory_method;

import com.netcracker.ec.khudnitsky.lect_04.factory_method.factories.Dialog;
import com.netcracker.ec.khudnitsky.lect_04.factory_method.factories.impl.HtmlDialog;
import com.netcracker.ec.khudnitsky.lect_04.factory_method.factories.impl.WindowsDialog;

/**
 * Производство кросс-платформенных элементов GUI
 * В этом примере в роли продуктов выступают кнопки, а в роли создателя — диалог.
 *
 * Разным типам диалогов соответствуют свои собственные типы элементов.
 * Поэтому для каждого типа диалога мы создаём свой подкласс и переопределяем в нём фабричный метод.
 *
 * Каждый конкретный диалог будет порождать те кнопки, которые к нему подходят.
 * При этом базовый код диалогов не сломается, так как он работает с продуктами только
 * через их общий интерфейс.
 *
 */
public class Runner {
  private static Dialog dialog;

  public static void main(String[] args) {
    configure();
    runBusinessLogic();
  }

  /**
   * Приложение создаёт определённую фабрику в зависимости от конфигурации или
   * окружения.
   */
  static void configure() {
    if (System.getProperty("os.name").equals("Windows 10")) {
      dialog = new WindowsDialog();
    } else {
      dialog = new HtmlDialog();
    }
  }

  /**
   * Весь остальной клиентский код работает с фабрикой и продуктами только
   * через общий интерфейс, поэтому для него неважно какая фабрика была
   * создана.
   */
  static void runBusinessLogic() {
    dialog.renderWindow();
  }
}
