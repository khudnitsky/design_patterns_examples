package com.netcracker.ec.khudnitsky.lect_04.abstract_factory.checkboxes;

/**
 * Чекбоксы — это второе семейство продуктов. Оно имеет те же вариации, что и
 * кнопки.
 */
public interface Checkbox {
  void paint();
}
