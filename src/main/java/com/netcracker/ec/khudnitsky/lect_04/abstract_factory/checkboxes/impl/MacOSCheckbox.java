package com.netcracker.ec.khudnitsky.lect_04.abstract_factory.checkboxes.impl;

import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.checkboxes.Checkbox;

/**
 * Все семейства продуктов имеют одинаковые вариации (MacOS/Windows).
 *
 * Вариация чекбокса под MacOS.
 */
public class MacOSCheckbox implements Checkbox {

  @Override
  public void paint() {
    System.out.println("You have created MacOSCheckbox.");
  }
}
