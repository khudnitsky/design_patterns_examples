package com.netcracker.ec.khudnitsky.lect_04.abstract_factory.factories.impl;

import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.buttons.Button;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.buttons.impl.MacOSButton;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.checkboxes.Checkbox;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.checkboxes.impl.MacOSCheckbox;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.factories.GUIFactory;

/**
 * Каждая конкретная фабрика знает и создаёт только продукты своей вариации.
 */
public class MacOSFactory implements GUIFactory {

  @Override
  public Button createButton() {
    return new MacOSButton();
  }

  @Override
  public Checkbox createCheckbox() {
    return new MacOSCheckbox();
  }
}
