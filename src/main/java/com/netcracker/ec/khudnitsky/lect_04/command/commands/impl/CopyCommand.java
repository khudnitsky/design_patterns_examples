package com.netcracker.ec.khudnitsky.lect_04.command.commands.impl;

import com.netcracker.ec.khudnitsky.lect_04.command.commands.Command;
import com.netcracker.ec.khudnitsky.lect_04.command.editor.Editor;

public class CopyCommand extends Command {

  public CopyCommand(Editor editor) {
    super(editor);
  }

  @Override
  public boolean execute() {
    editor.clipboard = editor.textField.getSelectedText();
    return false;
  }
}
