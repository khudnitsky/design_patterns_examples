package com.netcracker.ec.khudnitsky.lect_04.abstract_factory.factories;

import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.buttons.Button;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.checkboxes.Checkbox;

/**
 * Абстрактная фабрика знает обо всех (абстрактных) типах продуктов.
 */
public interface GUIFactory {
  Button createButton();
  Checkbox createCheckbox();
}
