package com.netcracker.ec.khudnitsky.lect_04.factory_method.buttons.impl;

import com.netcracker.ec.khudnitsky.lect_04.factory_method.buttons.Button;

/**
 * Реализация HTML кнопок.
 */
public class HtmlButton implements Button {

  public void render() {
    System.out.println("<button>Test Button</button>");
    onClick();
  }

  public void onClick() {
    System.out.println("Click! Button says - 'Hello World!'");
  }
}
