package com.netcracker.ec.khudnitsky.lect_04.builder.entities.components;

/**
 * Одна из фишек автомобиля.
 */
public enum Transmission {
  SINGLE_SPEED, MANUAL, AUTOMATIC, SEMI_AUTOMATIC
}
