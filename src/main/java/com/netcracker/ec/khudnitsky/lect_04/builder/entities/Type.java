package com.netcracker.ec.khudnitsky.lect_04.builder.entities;

public enum Type {
  CITY_CAR, SPORTS_CAR, SUV
}