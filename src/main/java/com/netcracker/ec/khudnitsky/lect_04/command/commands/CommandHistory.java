package com.netcracker.ec.khudnitsky.lect_04.command.commands;

import java.util.Stack;

public class CommandHistory {
  private Stack<Command> history = new Stack<>();

  public void push(Command c) {
    history.push(c);
  }

  public Command pop() {
    return history.pop();
  }

  public boolean isEmpty() { return history.isEmpty(); }
}
