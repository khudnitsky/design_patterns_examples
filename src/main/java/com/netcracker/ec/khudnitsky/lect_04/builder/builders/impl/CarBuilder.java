package com.netcracker.ec.khudnitsky.lect_04.builder.builders.impl;

import com.netcracker.ec.khudnitsky.lect_04.builder.builders.Builder;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.Car;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.Type;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.Engine;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.GPSNavigator;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.Transmission;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.components.TripComputer;

/**
 * Конкретные строители реализуют шаги, объявленные в общем интерфейсе.
 */
public class CarBuilder implements Builder {
  private Type type;
  private int seats;
  private Engine engine;
  private Transmission transmission;
  private TripComputer tripComputer;
  private GPSNavigator gpsNavigator;

  @Override
  public void setType(Type type) {
    this.type = type;
  }

  @Override
  public void setSeats(int seats) {
    this.seats = seats;
  }

  @Override
  public void setEngine(Engine engine) {
    this.engine = engine;
  }

  @Override
  public void setTransmission(Transmission transmission) {
    this.transmission = transmission;
  }

  @Override
  public void setTripComputer(TripComputer tripComputer) {
    this.tripComputer = tripComputer;
  }

  @Override
  public void setGPSNavigator(GPSNavigator gpsNavigator) {
    this.gpsNavigator = gpsNavigator;
  }

  public Car build() {
    return new Car(type, seats, engine, transmission, tripComputer, gpsNavigator);
  }
}
