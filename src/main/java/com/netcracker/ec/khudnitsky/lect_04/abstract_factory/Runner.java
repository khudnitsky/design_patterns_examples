package com.netcracker.ec.khudnitsky.lect_04.abstract_factory;

import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.app.Application;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.factories.GUIFactory;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.factories.impl.MacOSFactory;
import com.netcracker.ec.khudnitsky.lect_04.abstract_factory.factories.impl.WindowsFactory;

/**
 * Производство семейств кросс-платформенных элементов GUI
 * В этом примере в роли двух семейств продуктов выступают кнопки и чекбоксы.
 * Оба семейства продуктов имеют одинаковые вариации: для работы под MacOS и Windows.
 *
 * Абстрактная фабрика задаёт интерфейс создания продуктов всех семейств.
 * Конкретные фабрики создают различные продукты одной вариации (MacOS или Windows).
 *
 * Клиенты фабрики работают как с фабрикой, так и с продуктами только через абстрактные интерфейсы.
 * Благодаря этому, один и тот же клиентский код может работать с различными фабриками и продуктами.
 *
 */
public class Runner {

  /**
   * Приложение выбирает тип и создаёт конкретные фабрики динамически исходя
   * из конфигурации или окружения.
   */
  private static Application configureApplication() {
    Application app;
    GUIFactory factory;
    String osName = System.getProperty("os.name").toLowerCase();
    if (osName.contains("mac")) {
      factory = new MacOSFactory();
      app = new Application(factory);
    } else {
      factory = new WindowsFactory();
      app = new Application(factory);
    }
    return app;
  }

  public static void main(String[] args) {
    Application app = configureApplication();
    app.paint();
  }
}
