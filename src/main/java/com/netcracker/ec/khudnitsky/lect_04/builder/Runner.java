package com.netcracker.ec.khudnitsky.lect_04.builder;

import com.netcracker.ec.khudnitsky.lect_04.builder.builders.impl.CarBuilder;
import com.netcracker.ec.khudnitsky.lect_04.builder.builders.impl.CarManualBuilder;
import com.netcracker.ec.khudnitsky.lect_04.builder.directors.Director;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.Car;
import com.netcracker.ec.khudnitsky.lect_04.builder.entities.Manual;

/**
 * В этом примере Строитель позволяет пошагово конструировать различные конфигурации автомобилей.
 * Кроме того, здесь показано как с помощью Строителя
 * может создавать другой продукт на основе тех же шагов строительства.
 * Для этого мы имеем два конкретных строителя — CarBuilder и CarManualBuilder.
 * Порядок строительства продуктов заключён в Директоре.
 * Он знает какие шаги строителей нужно вызвать,
 * чтобы получить ту или иную конфигурацию продукта.
 * Он работает со строителями только через общий интерфейс,
 * что позволяет взаимозаменять объекты строителей для получения разных продуктов.
 *
 */
public class Runner {

  public static void main(String[] args) {
    Director director = new Director();

    // Директор получает объект конкретного строителя от клиента
    // (приложения). Приложение само знает какой строитель использовать,
    // чтобы получить нужный продукт.
    CarBuilder builder = new CarBuilder();
    director.constructSportsCar(builder);

    // Готовый продукт возвращает строитель, так как Директор чаще всего не
    // знает и не зависит от конкретных классов строителей и продуктов.
    Car car = builder.build();
    System.out.println("Car built:\n" + car.getType());


    CarManualBuilder manualBuilder = new CarManualBuilder();

    // Директор может знать больше одного рецепта строительства.
    director.constructSportsCar(manualBuilder);
    Manual carManual = manualBuilder.build();
    System.out.println("\nCar manual built:\n" + carManual.print());
  }

}
